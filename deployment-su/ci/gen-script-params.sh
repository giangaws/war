#!/bin/bash
RED='\033[0;31m'
NC='\033[0m' # No Color
stack_ids=$( find -maxdepth 1 -type f |sed -e 's/\.//g' |sed -e 's/\///g' |sed -e '1d' |grep yml | sed -e 's/yml/\.yml/g')
#stack_ids='globeone-es-ftw002.yml'
for i in $stack_ids
do
  echo -e "${RED}$i "
  functions_list=$( cat $i | awk 'c-->0;$0~s{if(b)for(c=b+1;c>1;c--)print r[(NR-c+1)%b];print;c=a}b{r[NR%b]=$0}' b=1 a=0 s="handler" | grep -v 'handler:'| cut -d ':' -f 1)
  for ii in $functions_list
  do
    echo -e ${NC}$ii |sed -e "s/"$ii"/'"$ii"',/g" 
  done
done
