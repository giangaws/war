pipeline{
    agent any
    stages{
        stage('Checkout') {
            steps {
                checkout([
                  $class: 'GitSCM',
                  branches: [
                    [
                      name: '*/goroam-develop'
                    ]
                  ],
                  doGenerateSubmoduleConfigurations: false,
                  extensions: [

                  ],
                  submoduleCfg: [

                  ],
                  userRemoteConfigs: [
                    [
                      credentialsId: '211938d0-1a0e-4096-89f8-cfb2159a0c4c',
                      url: 'https://gitlab.sutrix.com/devops/demo-goroam.git'
                    ]
                  ]
                ])
            }
        }
        stage('Build'){
            steps{
                dir ('/var/lib/jenkins/workspace/goroam-es-api-deploy/deployment/ci'){
                        sh "bash ./build_source.sh ${params.stackids} ${params.functions} "
                }      
            }
        }
        stage('Deploy'){
            environment{
                stage='dev'
                aws_profile='glesCreds'
            }
            steps{
                dir('/var/lib/jenkins/workspace/goroam-es-api-deploy/deployment/ci'){
                    sh 'rm -rf serverless.yml'
                    sh "cp ${params.stackids} serverless.yml"
                    script {
                        if ( "${params.functions}" == 'all') {
                            sh " echo 'deploy all' "
                            sh " echo serverless deploy -s ${stage} --aws-profile ${aws_profile}"
                            //sh " serverless deploy -s ${stage} --aws-profile ${aws_profile} "
                        } else {
                            sh "echo deploy function ${params.functions} "
                            sh "echo serverless deploy -s ${stage} function --function ${params.functions} --aws-profile ${aws_profile}"
                            //sh 'serverless deploy -s ${stage} function --function ${params.functions} --aws-profile ${aws_profile} '
                        }
                    }
                }
            }
        }
    }
}