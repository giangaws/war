#!bin/bash
workspace='../../'

path_deploy="$workspace/deployment/ci"
RED='\033[0;31m'
NC='\033[0m' # No Color
GREEN='\033[32m'

#put params stackid va function from jenkins
stackid=$1
function=$2

#if function is all, that will build all source
if [ "$2" == "all" ]
then
    cd $workspace
    dir_current=$(pwd)
    echo $dir_current
    echo "mvn clean package"
else 
    cd $path_deploy
    #find path source from info function_name in stackids
    info_func_fact=$(cat $stackid | grep -i -A 6 -w "$function:" |grep 'artifact' | cut -d '}' -f 2 |awk -F"target" '{print$1}' )
    #go to function's path to build
    cd $workspace$info_func_fact
    dir_current=$(pwd)
    echo $dir_current
    echo "mvn clean package"
fi 