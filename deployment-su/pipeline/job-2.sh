pipeline {
    agent any
    environment {
        workdir="/var/lib/jenkins/workspace/final-pipeline"
        iamgesid_maven_old = """${sh( returnStdout: true,script: 'docker images | grep maven | awk {\'print $3\'}')}""" 
        iamgesid_java_old = """${sh( returnStdout: true,script: 'docker images | grep java | awk {\'print $3\'}')}"""         
        name_container="maven-project"
   }
    stages {
        stage('Checkout') {
            steps {
                checkout([
                  $class: 'GitSCM',
                  branches: [
                    [
                      //name: '*/feature/integrate-dev-server'
					  //name: '*/release/0.2.0'
					  name: '*/master'
                    ]
                  ],
                  doGenerateSubmoduleConfigurations: false,
                  extensions: [

                  ],
                  submoduleCfg: [

                  ],
                  userRemoteConfigs: [
                    [
                      //credentialsId: 'da3be80c-0b88-4c33-9432-1aed4c6eebb4',
                      url: 'https://gitlab.com/giangaws/devops.git'
                    ]
                  ]
                ])
            }
        }
        stage('get new image maven and jdk') {
            steps {    
                sh "docker rmi -f ${iamgesid_maven_old}"
                sh "docker rmi -f ${iamgesid_java_old}"
            }
            post ('pull images maven and jdk') {
                always {
                    sh "docker pull maven"
                    sh "docker pull openjdk"
                }
            }
        }
        stage('build source') {
            environment {
                iamgesid_maven_new = """${sh(returnStdout: true,script: 'docker images | grep maven | awk {\'print $3\'}')}"""
                docker_run='docker run --tty -ti -d --rm --name ${name_container} -v ${workdir}:/usr/src/mymaven -w /usr/src/mymaven ${iamgesid_new} mvn clean install'
            }
            steps {
                sh "${docker_run}"
                sh "docker logs -f ${name_container}"
            }
        }
        stage('upload source'){
            steps {
                echo 'build source ok'
            }
            post('upload artifacts to s3'){
                success {
                    sh "awk s3 cp target/*.jar s3://sutrix-release/project-1"
                }
            }
        }
        stage('deployment on prod'){
            environment{
                prod_server='13.229.102.1'
            }
            steps{
                echo 'deploying prod'
            }
            post('download source from s3'){
                success{
                    sshagent(['e7ff756d-8ce2-4ac3-b0ff-c644c48dd11f']){
                        sh "ssh jenkins@${prod_server} 'sh rm -rf /opt/maven-source/*'"
                        sh "ssh jenkins@${prod_server} 'ssh aws s3 sync s3://sutrix-release/project-1 /opt/maven-source/'"
                        sh "ssh jenkins@${prod_server} 'ssh java -jar /opt/maven-source/devops-0.1.jar &'"
                    }
                }
            }
        }
        stage('health check'){
            environment{
                prod_server='13.229.102.1'
            }
            steps{
                echo 'checking port'
            }
            post('health check'){
                success{
                    sh "curl ${prod_server}:8080 "
                }
            }
        }
    }
}
