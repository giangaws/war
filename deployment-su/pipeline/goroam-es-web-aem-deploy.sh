pipeline {
    agent any
    stages {
        stage('Checkout') {
            steps {
                checkout([
                  $class: 'GitSCM',
                  branches: [
                    [
                      name: '*/develop'
                    ]
                  ],
                  doGenerateSubmoduleConfigurations: false,
                  extensions: [

                  ],
                  submoduleCfg: [

                  ],
                  userRemoteConfigs: [
                    [
                      credentialsId: '211938d0-1a0e-4096-89f8-cfb2159a0c4c',
                      url: 'https://gitlab.sutrix.com/su1/goroam/goroam-aem.git'
                    ]
                  ]
                ])
            }
        }

        stage('Build') {
    			steps {
    		        parallel (
    		            "aem.core" : {
    		                sh 'mvn clean install -Dskip.npm'
    		            }
    		        )
    		    }
        }

        stage('SonarQube Analysis') {
          steps {
            script {
              withSonarQubeEnv('Sonar-server49') {
                sh '/usr/src/sonar-scanner-3.0.3/bin/sonar-scanner -e ' +
                ' -Dsonar.host.url=http://10.0.1.49:9000 ' +
                ' -Dsonar.projectKey=goroam-aem -Dsonar.projectName=goroam-aem -Dsonar.projectVersion=1.0' +
                ' -Dsonar.sources=core/src/main/java/ph/com/globe/goroam ' +
                ' -Dsonar.language=java ' +
                ' -Dsonar.java.source=1.8 ' +
                ' -Dsonar.sourceEncoding=UTF-8 ' +
                ' -Dsonar.java.binaries=core/target/classes ' +
                ' -Dsonar.java.coveragePlugin=jacoco ' +
                ' -Dsonar.jacoco.reportPaths=core/target/jacoco.exec '
              }
            }
          }
        }

        stage('Quality Gate') {
          steps {
            script {
              timeout(time: 2, unit: 'MINUTES') {
                def qg = waitForQualityGate()
                if (qg.status != 'OK') {
                  error "Pipeline aborted due to quality gate failure: ${qg.status}"
                }
              }
            }
          }
        }

        stage('Clover Report') {
          steps {
            echo 'Clover Report'
            sh 'mvn clean install -Dskip.npm -PcodeQuality -Dspotbugs.failOnError=false -Dpmd.failOnViolation=false -Dmaven.clover.failOnViolation=false'

            step([
              $class: 'hudson.plugins.clover.CloverPublisher',
              cloverReportDir: 'core/target/site/clover',
              cloverReportFileName: 'clover.xml',
              healthyTarget: [methodCoverage: 70, conditionalCoverage: 70, statementCoverage: 70], // optional, default is: method=70, conditional=80, statement=80
              unhealthyTarget: [methodCoverage: 50, conditionalCoverage: 50, statementCoverage: 50], // optional, default is none
              failingTarget: [methodCoverage: 0, conditionalCoverage: 0, statementCoverage: 0]     // optional, default is none
            ])
          }
        }

        stage('Author Deploying') {
          steps {
		    parallel (
               "core" : {
                  sh 'mvn clean install -PautoInstallPackage -Daem.host=3.1.124.144 -Daem.port=10502 -Dvault.user=devops-ci -Dvault.password=zqCna6x7hzEs'
                }
            )
          }
        }

        stage('Publish Deploying 1') {
          steps {
		    parallel (
               "core" : {
                  sh 'mvn clean install -PautoInstallPackage -Daem.host=3.0.248.244 -Daem.port=10503 -Dvault.user=devops-ci -Dvault.password=zqCna6x7hzEs'
                }
            )
          }
        }

		stage('Publish Deploying 2') {
          steps {
		    parallel (
               "core" : {
                  sh 'mvn clean install -PautoInstallPackage -Daem.host=13.228.53.114 -Daem.port=10503 -Dvault.user=devops-ci -Dvault.password=zqCna6x7hzEs'
                }
            )
          }
        }
    }
}
