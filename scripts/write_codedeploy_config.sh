#!/bin/bash

set -e

mkdir -p /tmp/codedeploy/tomcat-sample

cat <<EOF >/tmp/codedeploy/tomcat-sample/env.properties
APPLICATION_NAME=$APPLICATION_NAME
DEPLOYMENT_GROUP_NAME=$DEPLOYMENT_GROUP_NAME
DEPLOYMENT_ID=$DEPLOYMENT_ID
EOF
