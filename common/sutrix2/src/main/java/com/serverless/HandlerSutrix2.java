package com.serverless;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Collections;

import org.apache.log4j.Logger;
import org.apache.log4j.BasicConfigurator;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class HandlerSutrix2 implements RequestHandler<Map<String, Object>, ApiGatewayResponse> {

	private static final Logger LOG = Logger.getLogger(HandlerSutrix2.class);

        public ApiGatewayResponse handleRequest(Map<String, Object> input, Context context) {
                LOG.info("received: " + input);
                Response responseBody = new Response("Hello Sutrix 2, the current time is " + new Date());
                Map<String, String> headers = new HashMap<>();
                headers.put("X-Powered-By", "AWS Lambda & Serverless");
                headers.put("Content-Type", "application/json");
                return ApiGatewayResponse.builder()
                                .setStatusCode(200)
                                .setObjectBody(responseBody)
                                .setHeaders(headers)
                                .build();
        }

}
