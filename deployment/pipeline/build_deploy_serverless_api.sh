pipeline{
    agent any
    stages{
        stage('Checkout') {
            steps {
                checkout([
                  $class: 'GitSCM',
                  branches: [
                    [
                      name: '*/master'
                    ]
                  ],
                  doGenerateSubmoduleConfigurations: false,
                  extensions: [

                  ],
                  submoduleCfg: [

                  ],
                  userRemoteConfigs: [
                    [
                      //credentialsId: 'da3be80c-0b88-4c33-9432-1aed4c6eebb4',
                      url: 'https://gitlab.com/giangaws/war.git'
                    ]
                  ]
                ])
            }
        }
        stage('Build'){
            steps{
                dir ('/var/lib/jenkins/workspace/demo/deployment/ci'){
                        sh "sh build_source.sh ${params.stackids} ${params.functions}"
                }      
            }
        }
        stage('Deploy'){
            environment{
                stage='dev'
                aws_profile='glesCreds'
            }
            steps{
                dir('/var/lib/jenkins/workspace/demo/deployment/ci'){
                    sh 'rm -rf serverless.yml'
                    sh "cp ${params.stackids} serverless.yml"
                    script {
                        if ( "${params.functions}" == 'all') {
                            sh " echo 'deploy all' "
                            sh " echo serverless deploy -s ${stage} --aws-profile ${aws_profile}"
                            //sh " serverless deploy -s ${stage} --aws-profile ${aws_profile} "
                        } else {
                            sh "echo deploy function ${params.functions} "
                            sh "echo serverless deploy -s ${stage} function --function ${params.functions} --aws-profile ${aws_profile}"
                            //sh 'serverless deploy -s ${stage} function --function ${params.functions} --aws-profile ${aws_profile} '
                        }
                    }
                }
            }
        }
    }
}