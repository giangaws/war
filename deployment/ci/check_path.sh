#!/bin/bash
RED='\033[0;31m'
NC='\033[0m' # No Color
GREEN='\033[32m'
stack_ids=$( find -maxdepth 1 -type f |sed -e 's/\.//g' |sed -e 's/\///g' |sed -e '1d' |grep yml | sed -e 's/yml/\.yml/g' | sort)
for i in $stack_ids
do
        echo -e $RED$i 
        functions_list=$( cat $i | awk 'c-->0;$0~s{if(b)for(c=b+1;c>1;c--)print r[(NR-c+1)%b];print;c=a}b{r[NR%b]=$0}' b=1 a=0 s="handler:" | grep -v 'handler:'| cut -d ':' -f 1)
        artifact_list=$(cat $i |grep artifact | cut -d '}' -f 2)
        for ii in $functions_list
        do
                echo -e $GREEN$ii
                #info_func_fact=$(cat $i | awk 'c-->0;$0~s{if(b)for(c=b+1;c>1;c--)print r[(NR-c+1)%b];print;c=a}b{r[NR%b]=$0}' b=0 a=4 s="$ii" | grep 'artifact'|cut -d '}' -f 2 )
                info_func_fact=$(cat $i | grep -i -A 6 -w "$ii:" |grep 'artifact' | cut -d '}' -f 2 |awk -F"target" '{print$1}' )
                echo -e $NC$info_func_fact
        done
done