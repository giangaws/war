pipeline {
    agent any
    stages {
        stage('Checkout') {
            steps {
                checkout([
                  $class: 'GitSCM',
                  branches: [
                    [
                      //name: '*/feature/integrate-dev-server'
					  //name: '*/release/0.2.0'
					  name: '*/feature/integrate-staging-server'
                    ]
                  ],
                  doGenerateSubmoduleConfigurations: false,
                  extensions: [

                  ],
                  submoduleCfg: [

                  ],
                  userRemoteConfigs: [
                    [
                      credentialsId: 'da3be80c-0b88-4c33-9432-1aed4c6eebb4',
                      url: 'https://gitlab.sutrix.com/su1/pinot-ep.git'
                    ]
                  ]
                ])
            }
        }
		stage('Copy File') {
            steps {
                dir('/var/lib/jenkins/workspace') {
                sh 'cp -r /home/sutrix_admin/ext-data/dev/database.properties /var/lib/jenkins/workspace/pinot-ep/extensions/database/ext-data/src/main/resources/environments/dev/'
				sh 'cp -r /home/sutrix_admin/devops/dev/ /var/lib/jenkins/workspace/pinot-ep/devops/pusher-package/environments/'
				sh 'cp -r /home/sutrix_admin/web.xml /var/lib/jenkins/workspace/pinot-ep/extensions/cortex/ext-cortex-webapp/src/main/webapp/WEB-INF'
                }
            }
        }
        stage('Build Extension Integration') {
			steps {
			    dir('extensions/core/') {
				sh 'mvn clean install -U'
				}
			}
		}
        stage('Build Extension') {
			steps {
			    dir('extensions/') {
				sh 'mvn clean install -U'
				}
			}
		}
		stage('Build Extension Package') {
			steps {
			    dir('extensions/packager/ext-deployment-package') {
				sh 'mvn clean install'
				}
			}
		}
        stage('Devops Pusher Package') {
            steps {
                dir('devops/pusher-package') {
                sh 'mvn clean install'
                }
            }
        }
        stage('Deployment') {
            steps {
                dir('devops/pusher-package') {
				sh 'ssh -tt ubuntu@52.76.37.229 "pwd; rm -rf target; rm -rf deploy; mkdir deploy"'
				sh 'scp target/*.zip ubuntu@52.76.37.229:deploy'
				sh 'ssh -tt ubuntu@52.76.37.229 "cd deploy; pwd; chmod +x *.zip; unzip *pusher-package*.zip"'
				sh 'ssh -tt ubuntu@52.76.37.229 "cd deploy/the-pusher; pwd; chmod +x PushDeploy.sh && ./PushDeploy.sh -p ../*deployment-package*.zip -f ../environments/dev/pusher.conf -f ../environments/dev/database.properties -d none"'
                }
            }
        }
    }
}
